from typing import List, Dict, Any

from engine.storage.Query import Query, IdentityFunc


class CompositeQuery(Query):
    """
    Stack of queries, each returning a list of crumbs with the possible exception of the last one.
    """

    def __init__(self,
                 queries: List[Query]):
        """
        :param queries: Queries returning a list of crumbs.
        """
        super(CompositeQuery, self).__init__(
            crumb_processing_func=IdentityFunc,
            state_processing_func=self.__helper_call
        )
        self.query_stack = queries

    def __helper_call(self,
                      crumbs: List[Dict[str, Any]]):
        """
        :param crumbs: List of crumbs.
        """
        for query in self.query_stack:
            crumbs = query(crumbs)
        return crumbs
