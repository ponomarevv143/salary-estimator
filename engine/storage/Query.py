from typing import List, Any, Callable, Union, Dict


class IdentityFunc:
    """
    Identity function. Shouldn't be called, instead use checks.
    """

    def __init__(self):
        pass

    def __call__(self, x: Any, *args, **kwargs):
        return x


class Query:
    """
    Base class for queries. To modify, identify custom crumb and state processing functions.
    """

    def __init__(self,
                 crumb_processing_func: Callable[[Dict[str, Any]], Any] = IdentityFunc,
                 state_processing_func: Callable[[List[Any]], Any] = IdentityFunc):
        self.crumb_processing_func = crumb_processing_func
        self.state_processing_func = state_processing_func

    def __call__(self,
                 crumbs: List[Dict[str, Any]],
                 *args,
                 **kwargs):
        # State = processed crumbs
        state = crumbs if self.crumb_processing_func == IdentityFunc \
            else [self.crumb_processing_func(crumb) for crumb in crumbs]
        # Processing the state
        processed_state = state if self.state_processing_func == IdentityFunc \
            else self.state_processing_func(state)
        return processed_state


class ConditionQuery(Query):
    """
    Selects crumbs based on a condition.
    State-immutable.
    """

    def __init__(self,
                 condition: Callable[[Dict[str, Any]], bool],
                 safe: bool = True):
        """
        :param safe: If False, replaces the call with a faster query that may contain None elements
        in the end result.
        """
        if safe:
            super(ConditionQuery, self).__init__(
                crumb_processing_func=lambda crumb: crumb if condition(crumb) else None,
                state_processing_func=lambda crumb_list: [crumb for crumb in crumb_list if crumb]
            )
        else:
            super(ConditionQuery, self).__init__(
                crumb_processing_func=lambda crumb: crumb if condition(crumb) else None
            )


class SelectQuery(ConditionQuery):
    """
    Simple select query.
    State-immutable.
    """

    def __init__(self,
                 key_field: str,
                 safe: bool = True):
        super(SelectQuery, self).__init__(
            condition=lambda crumb: key_field in crumb,
            safe=safe
        )


class ProjectionQuery(Query):
    """
    Selects a subset of information from a crumb.
    State-immutable.
    """

    def __init__(self,
                 keys: Union[str, List[str]]):
        keys = keys if type(keys) is list else [keys]
        super(ProjectionQuery, self).__init__(
            crumb_processing_func=lambda crumb: {key: value for key, value in crumb.items() if key in keys}
        )


class GroupByQuery(Query):
    """
    Groups the crumbs by a key field, mutating the structure.
    {                       {
        "key": A,       ->      "A": [...],
        "rest": ...,            "B": [...],
    }                       }
    State-mutable -- changes the state from List[Crumb] to Dict[Any, List[Dict[str, Any]]]
    """

    def __init__(self,
                 key_field: str,
                 drop_key_field: bool = True):
        self.key_field = key_field
        self.drop_key_field = drop_key_field
        super(GroupByQuery, self).__init__(
            state_processing_func=self.__grouping_function
        )

    def __grouping_function(self,
                            crumbs: List[Any]) -> Dict[Any, List[Dict[str, Any]]]:
        groups = {}
        for crumb in crumbs:
            if self.key_field in crumb:
                value = crumb[self.key_field]
                if self.drop_key_field:
                    del crumb[self.key_field]
                groups[value] = groups[value] + [crumb] if value in groups else [crumb]
        return groups


class ApplyQuery(Query):
    """
    Applies a function to a certain key field of the crumb.
    State-immutable -- crumbs stay crumbs.
    """
    def __init__(self,
                 key_field: str,
                 function: Callable[[Any], Any]):
        self.key_field = key_field
        self.function = function
        super(ApplyQuery, self).__init__(
            crumb_processing_func=self.__mutate_crumb
        )

    def __mutate_crumb(self, crumb):
        if self.key_field in crumb:
            crumb[self.key_field] = self.function(crumb[self.key_field])
        return crumb


class MinMaxQuery(Query):
    """
    State-mutable query, finding minimal and maximal element across a key field,
    conforming to a predefined order.
    Order is a function of two arguments - this and other, that is True when the condition
    holds and False otherwise. It is required that for any this =/= other order(this, other) = not order(other, this).
    Example: >(a, b) is equivalent to a > b.
    """

    def __init__(self,
                 key_field: str,
                 order: Callable[[Any, Any], bool]):
        self.key_field = key_field
        self.order = order
        super(MinMaxQuery, self).__init__(
            state_processing_func=self.__calculate_min_max
        )

    def __calculate_min_max(self,
                            crumbs: List[Dict[str, Any]]):
        _min, _max = None, None
        for crumb in crumbs:
            if self.key_field in crumb:
                if crumb[self.key_field]:
                    # Find min: if >(elem, previous_min), then min is previous_min
                    if _min:
                        _min = _min if self.order(crumb[self.key_field], _min) else crumb[self.key_field]
                    else:
                        _min = crumb[self.key_field]

                    # Find max: if >(elem, previous_max), then max is elem
                    if _max:
                        _max = crumb[self.key_field] if self.order(crumb[self.key_field], _max) else _max
                    else:
                        _max = crumb[self.key_field]
        return _min, _max
