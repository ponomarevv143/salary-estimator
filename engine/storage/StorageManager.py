import json
import os.path
from typing import Any, Dict, Union, List

from engine.Exceptions import StorageManagerException
from engine.storage.Crumb import Crumb
from engine.storage.Query import Query


class StorageManager:
    def __init__(self,
                 root_path: str = "data/",
                 crumb_path: str = "crumbs/",
                 filename: str = "crumbs",
                 crumb_filename: str = "",
                 initialize_from_existing: bool = True):
        self.root_path = root_path
        self.crumb_path = crumb_path
        self.filename = filename
        self.crumb_filename = crumb_filename
        self.__counter = 0
        self.__crumbs = []
        self.__build_directories()
        if initialize_from_existing:
            self.initialize_from_existing()

    def __build_directories(self) -> None:
        """
        Builds directories if they don't exist
        """
        if not os.path.exists(self.root_path):
            os.mkdir(self.root_path)
        if not os.path.exists(self.root_path + self.crumb_path):
            os.mkdir(self.root_path + self.crumb_path)

    def __collect(self) -> None:
        """
        Collects all crumbs in a list and writes them to root/filename.
        """
        if not self.is_empty():
            with open(self.root_path + self.filename + ".json", 'w') as collector:
                json.dump(list(map(Crumb.read, self.__crumbs)), collector)

    def __next__(self) -> Crumb:
        """
        Returns next empty crumb, or creates and yields one if the crumbs are empty.

        :return: An empty crumb.
        """
        for crumb in self.__crumbs:
            if crumb.is_empty():
                return crumb
        # No crumbs have been found - create new, empty one:
        crumb = Crumb(filename=self.crumb_filename + f"_{self.__counter}",
                      relative_path=self.root_path + self.crumb_path)
        self.__counter += 1
        self.__crumbs.append(crumb)
        return crumb

    def __iter__(self) -> Crumb:
        for crumb in self.__crumbs:
            yield crumb

    def clear(self,
              reset_counter: bool = False) -> None:
        """
        Deletes all accumulated crumbs and their respective files stored in the folder.

        :param reset_counter: If true, resets the counter.
        """
        for crumb in self:
            crumb.remove()
        self.__crumbs = []
        if reset_counter:
            self.__counter = 0

    def is_empty(self) -> bool:
        """
        :return: True if the storage consists of empty crumbs, or if each crumb is empty.
        """
        for crumb in self.__crumbs:
            if not crumb.is_empty():
                return False
        return True

    def save(self, data: Dict[Any, Any]) -> None:
        """
        Saves data in the next available crumb.

        :param data: Any serializable data structure.
        """
        next(self).save(data)

    def initialize_from_existing(self):
        """
        Initializes crumbs if the root path exists.
        Folder structure:
        root\
            crumbs\
                ...
        """
        if os.path.exists(self.root_path):
            for crumb in os.listdir(path=self.root_path + self.crumb_path):
                self.__crumbs.append(Crumb(filename=crumb, relative_path=self.root_path + self.crumb_path))
                self.__counter += 1

    def read(self,
             collect: bool = True,
             delete_upon_reading: bool = False,
             reset_counter: bool = False,
             **params):
        if collect:
            self.__collect()
        data = list(map(Crumb.read, self.__crumbs))[0]
        if delete_upon_reading:
            self.clear(reset_counter=reset_counter)
        return data

    def read_generator(self,
                       delete_upon_reading: bool = False):
        for crumb in self.__crumbs:
            yield crumb
            if delete_upon_reading:
                crumb.remove()

    def execute_query(self,
                      queries: Union[Query, List[Query], Dict[Any, Query]]) -> Union[Query, List[Query], Dict[Any, Query]]:
        """
        Executes passed queries.
        - If passed one query, returns the result of the expected type.
        - If passed a list of queries, returns the results in the order they were given.
        - If passed a dictionary, returns the dictionary with the same keys, but values being the results of the queries.
        :param queries: Query(ies) to execute.
        """
        crumb_data = self.read()
        if issubclass(type(queries), Query):
            return queries(crumb_data)
        elif type(queries) is list:
            return [query(crumb_data) for query in queries]
        elif type(queries) is dict:
            return {key: query(crumb_data) for key, query in queries.items()}
        else:
            raise StorageManagerException("Passed type is not one of {Query, List[Query], Dict[Any, Query].\n"
                                          f"Received: {type(queries)}")