import json
import os
from typing import Any, Dict


class Crumb:
    """
    A facade for working with each individual crumb file.
    Crumb file is a small piece of data that is stored in json format.
    """
    def __init__(self,
                 filename: str,
                 relative_path: str = ""):
        self.filename = filename[:-5] if filename.endswith('.json') else filename
        self.path = relative_path
        self.__build_path()

    def __build_path(self):
        if not os.path.exists(self.path):
            os.mkdir(self.path)

    def is_empty(self) -> bool:
        """
        True, if the associated crumb file does not currently exist
        (i.e. the crumb is ready to accept new input)
        :return: True if crumb_root/crumb does not exist, else False.
        """
        return not os.path.exists(self.path + self.filename + '.json')

    def save(self,
             data: Any) -> None:
        """
        Saves the data at the defined path in JSON format.
        By default, appends to the end.

        :param data: Any serializable data structure.
        """
        if data:
            self.__build_path()
            with open(self.path + self.filename + '.json', 'w') as out:
                json.dump(data, out)

    def read(self) -> Dict[Any, Any]:
        """
        Reads the data from the associated crumb file.

        :return: Dictionary built from the read JSON.
        :raises IOException: If the file does not exist.
        """
        with open(self.path + self.filename + '.json', 'r') as inp:
            return json.load(inp)

    def remove(self) -> None:
        """
        Removes the associated file.

        :raises IOException: If the file does not exist.
        """
        os.remove(self.path + self.filename + '.json')
