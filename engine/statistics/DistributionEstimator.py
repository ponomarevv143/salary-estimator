from typing import Callable, Any, Dict, List, Optional

from engine.SearchEngine import SearchEngine
from engine.storage.Query import GroupByQuery, Query
from engine.storage.CompositeQuery import CompositeQuery
import numpy as np


class DistributionEstimator:
    def __init__(self,
                 search_engine: SearchEngine,
                 salary_key: str = 'salary',
                 area_key: str = 'area'):
        self.search_engine = search_engine
        self.salary_key = salary_key
        self.area_key = area_key

    # @staticmethod
    # def __fill_gaps(salary_list: List[List[Optional[int]]]) -> np.ndarray:
    #     _min, _max = None, None
    #     for salary_min, salary_max in salary_list:
    #         # Find min
    #         if _min:
    #             if salary_min:
    #                 _min = _min if salary_min > _min else salary_min
    #         else:
    #             _min = salary_min
    #         # Find max
    #         if _max:
    #             if salary_max:
    #                 _max = _max if _max > salary_max else salary_max
    #         else:
    #             _max = salary_max
    #     return np.stack(list(map(lambda salary: [
    #         salary[0] if salary[0] else salary_min,
    #         salary[1] if salary[1] else salary_max
    #     ], salary_list)))

    def __get_salaries(self,
                       grouped_salaries: Dict[str, List[Dict[str, Any]]],
                       salaries_processing_func: Callable[[np.ndarray], Any] = None) -> Dict[str, np.ndarray]:
        fixed_data = {
            area: np.array([
                crumb[self.salary_key] for crumb in grouped_salaries[area]
            ]) for area in grouped_salaries.keys()
        }
        return {
            area: salaries_processing_func(data) for area, data in fixed_data.items()
        } if salaries_processing_func else fixed_data

    @staticmethod
    def __process_area(salary_array: np.ndarray) -> Dict[str, Any]:
        if filter(None, list(salary_array)):
            # Fill the gaps
            try:
                min_salary = min(filter(None, list(salary_array[:, 0])))
            except ValueError:
                min_salary = 0
            try:
                max_salary = max(filter(None, list(salary_array[:, 1])))
            except ValueError:
                max_salary = min_salary
            for i in range(len(salary_array)):
                salary_array[i, 0] = salary_array[i, 0] if salary_array[i, 0] else min_salary
                salary_array[i, 1] = salary_array[i, 1] if salary_array[i, 1] else max_salary
            salary_array = salary_array.astype(int)
            # Estimate 20th and 80th percentile (IQR)
            q_low = np.percentile(salary_array, q=20, axis=0)
            q_high = np.percentile(salary_array, q=80, axis=0)
            # Clear anomalies
            clear_data = []
            for point in salary_array:
                if np.all(point > q_low) and np.all(point < q_high):
                    clear_data.append(list(point))
            clear_data = np.stack(clear_data) if clear_data else np.array([[0, 0]])
            # Estimate mean and std to approximate the normal distribution
            return {
                "code": "OK",
                "mean": np.mean(clear_data, axis=0),
                "std": np.std(clear_data, axis=0),
                "median": np.median(clear_data, axis=0)
            }
        else:
            return {
                "code": "FAILED",
                "mean": None,
                "std": None,
                "median": None
            }

    def estimate(self):
        query = CompositeQuery(queries=[
            GroupByQuery(key_field=self.area_key),
            Query(state_processing_func=
                  lambda grouped_salaries: self.__get_salaries(grouped_salaries,
                                                               salaries_processing_func=self.__process_area))
        ])
        return self.search_engine.execute_query(query)
