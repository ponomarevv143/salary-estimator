class SearchException(Exception):
    pass


class StorageManagerException(Exception):
    pass


class SearchEngineException(Exception):
    pass