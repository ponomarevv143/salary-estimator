import os.path
from typing import Union, List, Type, Any, Tuple, Optional, Dict

from engine.Exceptions import SearchEngineException
from engine.search.SearchDirector import SearchDirector
from engine.search.SearchRobot import SearchRobot
from engine.search.VacancyPreprocessor import VacancyPreprocessor
from engine.storage.Query import Query
from engine.storage.StorageManager import StorageManager


class SearchEngine:
    """
    Provides an interface for fetching the vacancies, processing and storing them,
    as well as providing an interface for sending queries to the storage managers,
    or accessing each individual one.
    """
    def __init__(self,
                 vacancy_names: Union[str, List[str]],
                 initialize_from_existing: bool = True,
                 root_path: str = "data/",
                 crumb_path: str = "crumbs/",
                 filename: str = "crumbs",
                 crumb_filename: str = "",
                 num_workers: int = 4,
                 vacancy_preprocessor: VacancyPreprocessor = VacancyPreprocessor(),
                 worker_class: Type = SearchRobot,
                 **worker_parameters):
        self.root = root_path
        self.crumb_path = crumb_path
        self.num_workers = num_workers
        self.filename = filename
        self.crumb_filename = crumb_filename
        self.__search_director = SearchDirector(vacancy_names=vacancy_names,
                                                num_workers=num_workers,
                                                vacancy_preprocessor=vacancy_preprocessor,
                                                worker_class=worker_class,
                                                worker_parameters=worker_parameters)
        self.__storage_managers = {}
        self.__storage_manager_factory = \
            lambda vacancy_name: StorageManager(root_path=root_path + vacancy_name + "/",
                                                crumb_path=crumb_path,
                                                filename=filename,
                                                crumb_filename=crumb_filename)
        if initialize_from_existing:
            self.initialize_from_existing()

    def set_vacancy_names(self, vacancy_names: Union[str, List[str]]) -> None:
        """
        Sets the vacancy names to be fetched.

        :param vacancy_names: A string or a list of strings - the names of vacancies to be fetched.
        """
        self.__search_director.set_vacancy_names(vacancy_names)

    def get_vacancy_names(self) -> List[str]:
        """
        Returns the names of vacancies to be searched.

        :return: List of vacancy names.
        """
        return self.__search_director.vacancy_names

    def __build(self) -> None:
        """
        Builds the root directory and initializes the storage managers for each vacancy (vacancy managers).
        """
        if not os.path.exists(self.root):
            os.mkdir(self.root)
        self.__storage_managers = {
            vacancy_name: self.__storage_manager_factory(vacancy_name=vacancy_name)
            for vacancy_name in self.get_vacancy_names()
        }

    def initialize_from_existing(self):
        """
        Initializes the storage managers from existing root path.
        """
        if os.path.exists(self.root):
            self.__search_director.set_vacancy_names(
                list(set(self.__search_director.vacancy_names + os.listdir(self.root)))
            )
            self.__storage_managers = {
                vacancy_name: StorageManager(root_path=self.root + vacancy_name + "/",
                                             crumb_path=self.crumb_path,
                                             filename=self.filename,
                                             crumb_filename=self.crumb_filename)
                for vacancy_name in os.listdir(self.root)
            }

    def execute(self,
                num_pages: int) -> None:
        """
        For each vacancy, fetches num_pages pages of vacancies and passes them to
        the respective storage manager for storage.

        :param num_pages: Number of pages to fetch.
        """
        if num_pages < 0:
            raise SearchEngineException("Couldn't execute search - number of pages cannot be negative, "
                                        f"got {num_pages}")
        self.__build()
        for result, vacancy_name in self.__search_director.execute(num_pages):
            self.__storage_managers[vacancy_name].save(data=result)

    def read(self, **storage_manager_read_parameters) -> Dict[str, Any]:
        """
        Reads the whole database.

        :return: The dictionary of the form {vacancy name: vacancy data}.
        """
        return {
            vacancy_name: storage_manager.read(**storage_manager_read_parameters)
            for vacancy_name, storage_manager in self.__storage_managers.items()
        }

    def read_data(self, vacancy_name: str, **storage_manager_read_parameters) -> Any:
        """
        Reads data from a corresponding storage manager.

        :param vacancy_name: Storage manager key.
        :return: Vacancy data, stored in the corresponding storage manager.
        """
        return self.__storage_managers[vacancy_name].read(**storage_manager_read_parameters)

    def __getitem__(self, vacancy_name: str) -> Any:
        """
        A facade for read_data that does not allow for the custom read parameters.
        """
        return self.read_data(vacancy_name)

    def __iter__(self) -> Tuple[str, StorageManager]:
        """
        Yields names of vacancies stored and their corresponding storage managers.
        """
        for item in self.__storage_managers.items():
            yield item

    def execute_query(self,
                      queries: Union[Query, List[Query], Dict[Any, Query]],
                      vacancy_names: Optional[Union[str, List[str]]] = None) -> Dict[str, Any]:
        """
        Executes queries across all databases or a subset of them.

        :param queries: Queries to execute.
        :param vacancy_names: Which databases to target. If None, targets all.
        :return: Dictionary of the form {vacancy name: query result}
        """
        keys = vacancy_names if vacancy_names else list(self.__storage_managers.keys())
        keys = keys if type(keys) is list else [keys]
        return {
            vacancy_name: self.__storage_managers[vacancy_name].execute_query(queries)
            for vacancy_name in self.__storage_managers if vacancy_name in keys
        }
