from typing import Tuple, List, Dict, Any, Optional

import requests
import json

from engine.Exceptions import SearchException


class SearchRobot:
    """
    Worker used to fetch vacancies from a certain page of hh.
    """
    def __init__(self,
                 vacancy_name: str,
                 vacancies_per_page: int = 100,
                 **params):
        self.__vacancy_name = vacancy_name
        self.__params = {
            'text': vacancy_name,
            'page': -1,
            'currency': 'RUR',
            'per_page': vacancies_per_page,
            'only_with_salary': True
        }

    def __call__(self,
                 page: int,
                 *args,
                 **kwargs) -> Tuple[List[Optional[Dict[str, Any]]], str]:

        """
        Takes a page number (positive integers and zero) and produces a list of all fetched vacancy JSONs,
        or an empty list if none were fetched.

        :parameter page: Index of page to be fetched.
        :returns: A list of fetched vacancy JSONs, or an empty list if none were fetched, along with the vacancy name.
        :raises SearchException: if passed page number is negative.
        """
        if page < 0:
            raise SearchException("Page number cannot be negative")
        self.__params['page'] = page
        with requests.get('https://api.hh.ru/vacancies/', params=self.__params) as response:
            response_json = json.loads(response.text)
            if 'items' in response_json:
                return response_json['items'], self.__vacancy_name
            else:
                return [], self.__vacancy_name
