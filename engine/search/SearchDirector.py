import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
from typing import Union, List, Type, Tuple, Dict, Any, Optional

from engine.Exceptions import SearchException
from engine.search.SearchRobot import SearchRobot
from engine.search.VacancyPreprocessor import VacancyPreprocessor


class SearchDirector:
    """
    Coordinates fetching and preprocessing of the vacancies done concurrently.
    """

    def __init__(self,
                 vacancy_names: Union[str, List[str]],
                 num_workers: int = 4,
                 vacancy_preprocessor: VacancyPreprocessor = VacancyPreprocessor(),
                 worker_class: Type = SearchRobot,
                 **worker_parameters):
        self.set_vacancy_names(vacancy_names)
        if num_workers < 1:
            raise SearchException("Couldn't instantiate SearchDirector - number of workers must be at least 1.")
        self.num_workers = num_workers
        self.vacancy_preprocess = vacancy_preprocessor
        self.worker_factory = lambda vacancy_name, worker_parameters=worker_parameters: \
            worker_class(vacancy_name, **worker_parameters)

    def execute(self, num_pages: int) -> Optional[Tuple[str, Dict[str, Any]]]:
        """
        Starts yielding fetched preprocessed jsons.

        :parameter num_pages: Number of pages to fetch.
        :returns: Yields vacancy result and vacancy result, conforming to the output type of passed VacancyPreprocessor.
        """
        if num_pages < 0:
            raise SearchException("Number of pages cannot be negative.")
        with ThreadPoolExecutor(max_workers=self.num_workers) as director:
            futures = []
            for vacancy_name in self.vacancy_names:
                for page in range(num_pages):
                    future = director.submit(self.worker_factory(vacancy_name), page=page)
                    futures.append(future)

            for future in concurrent.futures.as_completed(futures):
                try:
                    result, vacancy_name = future.result()
                    yield self.vacancy_preprocess(result), vacancy_name
                except Exception:
                    continue

    def set_vacancy_names(self, vacancy_names: Union[str, List[str]]) -> None:
        self.vacancy_names = vacancy_names if type(vacancy_names) is list else [vacancy_names]
