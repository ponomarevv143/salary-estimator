from typing import Callable, Any, Union, Dict, List, Optional


class VacancyPreprocessor:
    """
    Applies preprocessing to the json sent by the search robot.
    Preprocessing may be either custom or default.
    """
    def __init__(self,
                 preprocessing_map: Callable[[Union[Dict[str, Any], List[Dict[str, Any]]]], List[Any]] = None):
        self.preprocessing_map = preprocessing_map if preprocessing_map else self.__preprocessing_function

    @staticmethod
    def __preprocessing_function(vacancy_jsons: Union[Dict[str, Any], List[Dict[str, Any]]]) \
            -> List[Optional[Dict[str, Any]]]:
        """
        Simple preprocessing, fetching only certain parameters in the form:
        {
            salary: [from, to], // int values of salary
            area: area // lower string value of the work area
        }

        :param vacancy_jsons: List of fetched jsons, or an individual json (dict).
        :return: List of processed jsons.
        """
        vacancy_jsons = vacancy_jsons if type(vacancy_jsons) is list else [vacancy_jsons]
        return [{
            'salary': [vacancy_json['salary']['from'], vacancy_json['salary']['to']],
            'area': vacancy_json['area']['name'].lower(),
            'currency': vacancy_json['salary']['currency']
        } for vacancy_json in vacancy_jsons if vacancy_json]

    def __call__(self,
                 vacancy_jsons: Union[Dict[str, Any], List[Dict[str, Any]]],
                 *args,
                 **kwargs) -> List[Optional[Dict[str, Any]]]:
        """
        Preprocesses the list of jsons, or an individual json, for future use.
        Default preprocessing fetches only certain parameters in the form:
        {
            salary: [from, to], // int values of salary
            area: area // lower string value of the work area
        }

        :param vacancy_jsons: List of fetched jsons, or an individual json (dict).
        :return: List of processed jsons.
        """
        return self.preprocessing_map(vacancy_jsons)
